package com.nesterov;

public class RabinKarpAlgorithm {

    // Объявляем метод search, который будет искать все вхождения шаблона в тексте.
    public static void search(String text, String pattern) {
        // Получаем длину текста.
        int n = text.length();
        // Получаем длину шаблона.
        int m = pattern.length();
        // Задаем основание для хэша (размер алфавита).
        int d = 256;

        // Задаем простое число для вычисления хэша.
        int q = 101;

        // Вычисляем значение h для вычисления хэша.
        int h = 1;
        for (int i = 0; i < m - 1; i++) {
            h = (h * d) % q;
        }

        // Инициализируем переменные p и t для хранения хэшей шаблона и текущей подстроки текста.
        int p = 0;
        int t = 0;

        // Вычисляем хэши шаблона и первой подстроки текста.
        for (int i = 0; i < m; i++) {
            p = (d * p + pattern.charAt(i)) % q;
            t = (d * t + text.charAt(i)) % q;
        }

        // Проверяем все подстроки текста длиной m на совпадение с шаблоном.
        for (int i = 0; i <= n - m; i++) {
            // Если хэши совпадают, выполняем дополнительную проверку на полное совпадение символов.
            if (p == t) {
                boolean match = true;
                for ( int j = 0; j < m; j++) {
                    if (text.charAt(i + j) != pattern.charAt(j)) {
                        match = false;
                        break;
                    }
                }
                // Если все символы совпадают, выводим сообщение о позиции, с которой начинается совпадение.
                if (match) {
                    System.out.println("Pattern found at position " + i);
                }
            }

            // Если не все символы совпадают, пересчитываем хэш текущей подстроки для следующей позиции.
            if (i < n - m) {
                t = (d * (t - text.charAt(i) * h) + text.charAt(i + m)) % q;

                // Если хэш отрицательный, добавляем q.
                if (t < 0) {
                    t = t + q;
                }
            }
        }
    }

    // Основной метод программы.
    public static void main(String[] args) {
        // Задаем текст и шаблон для поиска.
        String text = "ABABDABACDABABCABAB";
        String pattern = "ABABCABAB";

        // Вызываем метод search для поиска шаблона в тексте.
        search(text, pattern);
        System.out.println();
    }
}
